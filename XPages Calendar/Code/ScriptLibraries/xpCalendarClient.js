var XCal = function(){
		
	var xcal = {
		items: [],
		startHours: [],
		entryDivs: [],
		_getData: function() {			
			var _this = this;
			var url = _this._getDataStoreUrl();
			console.log("url = ",url);
			var items = [];
			dojo.xhrGet({
				url: url,
				handleAs: "json",
				load: function(resp, ioArgs) {
					if (_this.items.length > 0) {
						_this.items = [];
					}
					if (_this.startHours.length > 0) {
						_this.startHours = [];
					}
					if (_this.containerDivs.length > 0) {
						_this.containerDivs = [];
					}
					dojo.forEach(resp, function(item){
						var sTime = new Date(item.StartTime);
						var sDate = new Date(item.StartDate);
						var startDateTime = new Date(sDate.toDateString() + " " + sTime.toTimeString());
						item.StartDateTime = startDateTime;
						item.StartTime = sTime.toTimeString();
						
						var eTime = new Date(item.EndTime);
						var eDate = new Date(item.EndDate);
						var endDateTime = new Date(eDate.toDateString() + " " + eTime.toTimeString());
						item.EndDateTime = endDateTime;
						item.EndTime = eTime.toTimeString();
						
						item.startHour = item.StartDateTime.getHours();
						item.startMinutes = item.StartDateTime.getMinutes();
						item.endMinutes = item.EndDateTime.getMinutes();
						_this.startHours.push(item.startHour);
						_this.items.push(item);
					});
					console.log(_this.items);
				},
				error: function(err, ioArgs) {
					console.error(err,ioArgs);
				}
			});
		},
		_getDataStoreUrl: function() {
			var url = "http://" + location.hostname;
			url += calDataStore.target;
			url += "?" + calDataStore.extraArgs;
			return url;
		},
		_entOnClick: function(xpage, unid) {
			var path = location.pathname.split('.nsf')[0] + '.nsf/';
			if (unid == "") {
				window.location.href = path + xpage + "?OpenXpage";
			}else{
				window.location.href = path + xpage + "?OpenXpage&unid=" + unid;
			}
		},
		_updateSessionScope:function() {
			console.log('updating sessionScope.dispDate');
			var dateSent = '#{javascript:setSessScope("dispDate","DispDate","date",true);}';
			return dateSent;
		},
		_getNumStartHours: function(startHour) {
			var startHours = this.startHours;
			var allStartHours = 0;
			if (startHours.length > 0) {
				console.log("startHours = ",startHours);
				for(var i = 0;i < startHours.length;i++) {
					if (startHours[i] == startHour) {
						allStartHours++;
					}
				}
			}
			return allStartHours;
		},
		buildDayEntries: function() {
			var _this = this;
			var items = _this.items;
			if (items.length > 0) {
				// Let's start building a div for each entry
				var preContId = "dayTimeMinutesContainer-";
				var counter = 0;
				dojo.forEach(items,function(item){
					var startHourStr = item.startHour.toString();
					var startHourInt = item.startHour;
					var containerId = preContId + startHourStr;
					var preEntryDivId = "dayTimeMinutes-" + startHourStr;
					var entryDivId = preEntryDivId;
					if (_getNumStartHours(startHourInt) > 1) {
						counter++;
						entryDivId = entryDivId + "-" + counter.toString();
					}
					var entryDiv = dojo.create("div",{id:entryDivId},containerId);
					entryDiv.onclick = function(){
						alert("Clicked " + item.ToolTip);
					};
					_setEntryStyle(entryDivId, startHourInt);
					
				});
			}else{
				console.log("No Items to process");
			}
		},
		_setEntryStyle: function(entryDivId, startHourInt, entryNumInContainer) {
			var containerId = "dayTimeMinutesContainer-" + startHourInt.toString();
			var width = dojo.coords(dojo.byId(containerId)).w;
			var left = dojo.coords(dojo.byId(containerId)).x;
			if (_getNumStartHours(startHourInt) > 1) {
				width = width / _getNumStartHours(startHourInt);
				left = width * entryNumInContainer;
			}
			dojo.style(entryDivId,{
				"position": "absolute",
				"left": left + "px",
				"width": width + "px",
				"z-index": 0
			});
		},
		_buildEntrySurface: function(entryDivId) {
			var entColor = {
				type: "linear",
				x1: 0, 
				y1: 540,
				x2: 0,
				y2: 0,
				colors: [
				         {offset:0, color:[207,241,176]},
				         {offset:1, color:[209,239,198]}
				]
			};
			//Determine the top of the entry
			var xyCoords = dojo.coords(dojo.byId(entryDivId));
			
		},
		addOnLoad: function(calEntries) {
			console.log('Starting addOnLoadFunc');
			//Get all the nodes named calEntry which are children of the
			//contextual node (an XML section containing the data we need. 
			//This contains all of our data UNID, startDate, startTime, endDate, 
			//endTime,duration,subject and toolTip
			var calEntries = dojo.query('calEntry',dojo.byId('contextual'));
			
			//Build an array containing everything we need. Start Date/Time, End Date/Time,
			// duration, subject, toolTip, the hours that this entry spans and the div name.	
			var allEntries = new Array();
			var allHours = new Array();	
			for (i = 0;i < calEntries.length;i++) {	
				allEntries[i] = new Array();		
				allEntries[i][0] = dojo.attr(calEntries[i], 'UNID');
				allEntries[i][1] = new Date(dojo.attr(dojo.query('startDate',calEntries[i])[0], 'value') + " " + dojo.attr(dojo.query('startTime',calEntries[i])[0], 'value'));
				allEntries[i][2] = new Date(dojo.attr(dojo.query('endDate',calEntries[i])[0], 'value') + " " + dojo.attr(dojo.query('endTime',calEntries[i])[0], 'value'));
				allEntries[i][3] = dojo.attr(dojo.query('duration',calEntries[i])[0], 'value');
				allEntries[i][4] = dojo.attr(dojo.query('subject',calEntries[i])[0], 'value');
				allEntries[i][5] = dojo.attr(dojo.query('toolTip',calEntries[i])[0], 'value');
				allEntries[i][6] = allEntries[i][1].getHours();
				allEntries[i][7] = allEntries[i][2].getHours();
				allEntries[i][8] = allEntries[i][1].getMinutes();
				//Determine the hours this entry spans		
				for (j = allEntries[i][6];j <= allEntries[i][7];j++) {
					if (j == allEntries[i][6]) {
						allEntries[i][9] = j;
					}else{
						if (allEntries[i][2].getMinutes() == 0 && j < allEntries[i][7]) {				
							allEntries[i][9] = allEntries[i][9] + "," + j;
						}else if (allEntries[i][2].getMinutes() > 0 && allEntries[i][3] >= (60 - allEntries[i][2].getMinutes())) {
							allEntries[i][9] = allEntries[i][9] + "," + j;
						}
					}
				}
				//Figure out the div id for this entry. We also need to determine
				// how many entries are in this hour so we know what size and
				// location later on for the div.
				//For some reason this only works for up to 2 divs within the same
				// hour. I think it's because I rename the div id so it's not found
				// when it gets to the 3rd one.
				allHours[i] = allEntries[i][6];
				var srch = allHours[i];
				var counter = 1;
				allEntries[i][11] = counter;
				if (i > 0) {
					if (allHours.indexOf(srch) > -1) {
						if (allHours.indexOf(srch) != i) {								
							var hourIndex = allHours.indexOf(srch);					
							while (hourIndex != -1) {
								allEntries[hourIndex][11] = allEntries[hourIndex][11] + 1;
								allHours[hourIndex] = allHours[hourIndex] + "-" + (counter - 1);
								hourIndex = allHours.indexOf(srch);						
								counter = counter + 1;
							}
						}
					}
				}
				allEntries[i][10] = "dayTimeMinutes-" + allHours[i];		
			}
			console.log("allEntries = ",allEntries);
			console.log("allHours = ",allHours);
			//alert("done with 1st loop \n " + allEntries);
			
			//Now that we've got all the information we need we can start building
			// divs for all the entries.
			console.log('start building divs');
			for (i = 0;i < allEntries.length;i++) {
				//Build a div for this entry		
				var containerID = "dayTimeMinutesContainer-" + allEntries[i][6];
				var contSubj = allEntries[i][5];
				dojo.query(dojo.create("div",{id:allEntries[i][10]},containerID))
					.onclick(function(){ 
						alert(contSubj);
					});		
			}
			
			//Set the style of all the single divs within a container
			console.log('set the style of the divs');
			var multiDivs = new Array();
			for (i = 0;i < allEntries.length;i++) {
				var containerID = "dayTimeMinutesContainer-" + allEntries[i][6];
				var containerCoords = dojo.coords(dojo.byId(containerID));
				var containerWidth = (containerCoords.w/allEntries[i][11]);		
				
				//Determine the X coordinates. If there are more than 1
				// div in the container we'll set the style after this loop
				// for each of the other divs
				var containerX = containerCoords.x;
				var containerDivs = dojo.query('#' + containerID + ' > *');		  
				if (containerDivs.length == 1) {
					dojo.style(allEntries[i][10],{
						"position":"absolute",
						"left":containerX + "px",
						"width":containerWidth + "px",
						"z-index":"0"
					});
				}else{
					if (multiDivs.indexOf(containerID) == -1) {
						multiDivs.push(containerID);
					}
				}
			}

			//Set the style for all the container divs which contain more than
			// 1 div
			console.log('set the style for container divs with more than 1 entry');
			for (i = 0;i < multiDivs.length;i++) {
				var containerCoords = dojo.coords(dojo.byId(multiDivs[i]));
				var minuteDivs = dojo.query('#' + multiDivs[i] + ' > *');
				var containerWidth = (containerCoords.w/minuteDivs.length);
				var containerX;		
				for (j = 0;j < minuteDivs.length;j++) {
					if (j == 0) {
						containerX = containerCoords.x;
					}else{
						containerX = (containerX + (containerCoords.w/minuteDivs.length));
					}			
					dojo.style(dojo.attr(minuteDivs[j], 'id'),{
						"position":"absolute",
						"left":containerX + "px",
						"width":containerWidth + "px",
						"z-index":"0"
					});
				}
			}
			
			//Now place a surface and rectangle within each minute div. We also
			// need to set the "top" style attribute for each minute div
			for (i = 0;i < allEntries.length;i++) {
				var entColor = { 
					type:"linear", 
					x1:0, y1:540, 
					x2:0, y2:0, 
					colors:[ 
						{offset:0,color:[207,241,176]},
						{offset:1,color:[209,239,198]} 
					] 
				};
				
				//Set the "top" style attribute dependent on the minutes	
				var xyCoords = dojo.coords(dojo.byId(allEntries[i][10]));
				if (allEntries[i][8] > 0) {
					dojo.style(allEntries[i][10],"top",(xyCoords.y + allEntries[i][8]) + "px");
				}else{
					dojo.style(allEntries[i][10],"top",xyCoords.y + "px");
				}
				
				//Create a surface in the appropriate div
				var surface = dojox.gfx.createSurface(allEntries[i][10],xyCoords.w,allEntries[i][3]);
				var group = surface.createGroup();
				var rect = group.createRect({x:2, y:0, width:(xyCoords.w-2), height:allEntries[i][3], r:5})
					.setFill(entColor)
					.setStroke({type:"stroke", color:[161,209,151], style:"solid", width:1});		
				var rectTxt = allEntries[i][4] + "-" + allEntries[i][6] + ":" + allEntries[i][8] + " to " + allEntries[i][7] + ":" + allEntries[i][2].getMinutes();
				var t = group.createText({x:5,y:12,text:rectTxt})
					.setFont({family:"Arial",size:"9pt",weight:"normal"})
					.setFill("black");
			}
			console.log('done with addOnLoadFunc');
		}
	}
	return xcal;
}();