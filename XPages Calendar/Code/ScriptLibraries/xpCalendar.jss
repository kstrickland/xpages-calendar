/**
 * build the navigation menu that is on the month calendar
 */
function buildMenu(dayPage,weekPage,dayNum) {
	if (dayPage.search(".xsp") == -1) {
		dayPage = dayPage + ".xsp"
	}
	if (weekPage.search(".xsp") == -1) {
		weekPage = weekPage + ".xsp"
	}
	var html = '<ul id="qm0" class="qmmc">';
	html = html + '<li><a class="qmparent" href="#">&nbsp;</a>';
	html = html + '<ul>';
	html = html + '<li><a href="' + dayPage + '?OpenXpage&calDay=' + dayNum + '">Day View</a></li>';
	html = html + '<li><a href="' + weekPage + '?OpenXpage&calDay=' + dayNum + '">Week View</a></li>';
	html = html + '</ul>';
	html = html + '</li></ul>';
	return html;
}
/**
 * Get the month/week number of the passed date as an integer
 */
function getMonthWeekNum(dateTime) {
	var wkDay;
	var tempWkDay;
	var curMonth;
	var workDate;
	var tempWeekNum;
	
	wkDay = dateTime.getDay();
	curMonth = dateTime.getMonth();
	
	//Set the date to the first of the month
	workDate = new Date(dateTime.getFullYear(),dateTime.getMonth(),1);
	tempWkDay = workDate.getDay();
	if (wkDay > tempWkDay) {
		workDate.setDate(wkDay-tempWkDay);
	}else if (wkDay < tempWkDay) {
		workDate.setDate(wkDay-tempWkDay);
	}
	
	//Figure out if the first of the month is the weekday we're looking for, if not then adjust
	if (workDate.getMonth() < curMonth) {
		tempWeekNum = 0
	}else{
		tempWeekNum = 1
	}
	
	//loop through incrementing the workDate by 7 days until it
	//equals dateTime
	if (workDate.toDateString() == dateTime.toDateString()) {
		tempWeekNum = 1
	}else{
		do {
			tempWeekNum = tempWeekNum + 1;
			workDate.setDate(workDate.getDate()+7);
		} 
		while (workDate.valueOf() <= dateTime.valueOf())
	}
	return tempWeekNum;
}
/**
 * Get the time difference between 2 dates in days
 */
function timeDayDifference(endDate,startDate) {
	var difference = endDate.getTime() - startDate.getTime();	
	difference -= (Math.floor(difference/1000/60/60/24)*1000*60*60*24);
	return difference;
}
/**
 * Get the time difference between 2 dates in hours
 */
function timeHourDifference(endDate,startDate) {
	var difference = endDate.getTime() - startDate.getTime();
	difference -= (Math.floor(difference/1000/60/60)*1000*60*60);
	return difference;
}
/**
 * Get the time difference between 2 dates in minutes
 */
function timeMinuteDifference(endDate,startDate) {
	var difference = endDate.getTime() - startDate.getTime();
	difference -= (Math.floor(difference/1000/60)*1000*60);
	return difference;
}
/**
 * Get the time difference between 2 dates in seconds
 */
function timeSecondDifference(endDate,startDate) {
	var difference = endDate.getTime() - startDate.getTime();
	difference = Math.floor(difference/1000);
	return difference;
}